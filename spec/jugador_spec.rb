require 'rspec' 
require_relative '../model/jugador'

describe 'Jugador' do

	let(:jugador1) { Jugador.new("Nicolas") }
	
	it 'El Nombre del jugador1 es Nicolas' do
	  expect(jugador1.nombre).to eq "Nicolas"
	end
	
	it 'El Nombre del jugador1 es Nicolas y lo cambio por Pedro' do
	 jugador1.set_nombre("Pedro")
	expect(jugador1.nombre()).to eq "Pedro"
	end

end