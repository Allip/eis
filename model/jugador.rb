class Jugador

	def initialize(nombre)
	  @nombre = nombre
	end
	
	def set_nombre(nombre)
		@nombre = nombre
	end

	def nombre()
		@nombre
	end
end